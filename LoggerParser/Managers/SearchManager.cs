﻿using LoggerParser.Constnats;
using LoggerParser.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace LoggerParser.Managers
{
    public static class SearchManager
    {
        public static List<Result> Search(string searchText, string filePath)
        {
            List<Result> resultList = new List<Result>();
            using (StreamReader reader = new StreamReader(filePath))
            {
                Pid pid = new Pid(1);
                Result result = new Result(-1, -1, null);
                List<string> pidLines = new List<string>();

                bool isFind = false;

                string line;
                int row = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    row++;
                    if (line.Contains(searchText))
                    {
                        isFind = true;
                        result = new Result(row, line.IndexOf(searchText) + 1, filePath);
                    }

                    if (pidLines.Count() > 0 && pidLines[0].Contains(PidContstants.PidStartWord) && line.Contains(PidContstants.PidStartWord))
                    {
                        if (isFind)
                            FillResultList(string.Join("\n", pidLines), ref pid, ref result, ref resultList);

                        pidLines.Clear();
                        pid = new Pid(row);
                        isFind = false;
                    }
                    pidLines.Add(line);
                }

                if (isFind)
                    FillResultList(string.Join("\n", pidLines), ref pid, ref result, ref resultList);
            }
            return resultList;
        }
        private static void FillResultList(string text, ref Pid pid, ref Result result, ref List<Result> results)
        {
            pid.Text = text;
            result.Pid = pid;
            results.Add(result);
        }

    }
}