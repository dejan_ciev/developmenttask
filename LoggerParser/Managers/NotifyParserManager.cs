﻿using LoggerParser.Hubs;
using LoggerParser.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LoggerParser.Managers
{
    public static class NotifyParserManager
    {
        public static void Notify(List<Result> results)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<ParserHub>();
            if (hubContext != null)
            {
                hubContext.Clients.All.showResult(results);
            }
        }
    }
}