﻿using LoggerParser.Helpers;
using LoggerParser.Managers;
using LoggerParser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoggerParser.Controllers.api
{
    public class ValueController : ApiController
    {
        private List<Result> results = new List<Result>();

        [HttpPost]
        public IHttpActionResult Search(SearchFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string[] filesPath = DirectoryHelpers.GetFilesPath(model.DirectoryPath);
            foreach (var filePath in filesPath)
            {
                results.AddRange(SearchManager.Search(model.SearchText, filePath));
            }
            NotifyParserManager.Notify(results);
            return Ok(true);
        }

        [HttpGet]
        public IHttpActionResult GetPid(string path, int row, int col)
        {
            Result result = results.Where(x => x.FilePath.Equals(path) && x.ResultRow == row && x.ResultColumn == col).FirstOrDefault();

            return Ok(result);
        }


    }
}
