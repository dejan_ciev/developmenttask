﻿using LoggerParser.Helpers;
using LoggerParser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace LoggerParser.Controllers
{
    public class HomeController : Controller
    {

  
        public ActionResult Index()
        {
            SearchFormModel model = new SearchFormModel
            {
                Directories = DropDownHelpers.GetDirectorieNameAndPath(HttpContext.Server.MapPath("~/Logs"))
            };
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

    }
}