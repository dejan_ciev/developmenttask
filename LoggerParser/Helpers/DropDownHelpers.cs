﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoggerParser.Helpers
{
    public static class DropDownHelpers
    {
        public static IEnumerable<SelectListItem> GetDirectorieNameAndPath(string rootPaht)
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            Dictionary<string, string> namePathPairs = new Dictionary<string, string>();
            string[] paths = Directory.GetDirectories(rootPaht);
            foreach (var path in paths)
            {
                var pathElements = path.Split('\\');

                string name = Path.GetFileName(Path.GetDirectoryName(path));
                SelectListItem item = new SelectListItem() {
                    Value = path,
                    Text = pathElements[pathElements.Length-1]
                };
                listItems.Add(item);

            }
            return listItems;
        }
    }
}