﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace LoggerParser.Helpers
{
    public static class DirectoryHelpers
    {
        public static string[] GetFilesPath(string root)
        {
            return Directory.GetFiles(root);
        }
    }
}