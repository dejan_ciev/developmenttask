﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LoggerParser.Models
{
    public class Pid
    {
        public Pid(int startRowNumber)
        {
            StartRowNumber = startRowNumber;
        }
        public int StartRowNumber { get; }

        public string Text { get; set; }

    }
}