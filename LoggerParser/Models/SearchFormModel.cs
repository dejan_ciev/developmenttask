﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoggerParser.Models
{
    public class SearchFormModel
    {
        [Display(Name ="Directory")]
        public string DirectoryPath { get; set; }

        [Display(Name ="Search text")]
        public string SearchText { get; set; }


        public IEnumerable<SelectListItem> Directories { get; set; }
    }
}