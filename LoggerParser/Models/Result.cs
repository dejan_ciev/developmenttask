﻿using LoggerParser.Constnats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LoggerParser.Models
{
    public class Result
    {
        public Result(int restultRow, int resultColumn, string filePath)
        {
            ResultRow = restultRow;
            ResultColumn = resultColumn;
            FilePath = filePath;
        }

        public Pid Pid { get; set; }
        public int ResultRow { get; }
        public int ResultColumn { get; }
        public string FilePath { get; }

        public string Href { get { return $"{PathConstants.ApiPathResultPid}?path={FilePath}&row={ResultRow}&col={ResultColumn}"; } }

        public string HrefName
        {
            get { return $"{FilePath}_"; }
        }
    }
}